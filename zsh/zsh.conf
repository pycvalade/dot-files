# compinit
if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

  autoload -Uz compinit
  compinit
fi

# transient prompt
zle-line-init() {
  emulate -L zsh

  [[ $CONTEXT == start ]] || return 0

  while true; do
    zle .recursive-edit
    local -i ret=$?
    [[ $ret == 0 && $KEYS == $'\4' ]] || break
    [[ -o ignore_eof ]] || exit 0
  done

  local saved_prompt="$PROMPT"
  local saved_rprompt=$RPROMPT
  PROMPT="%K{white}%F{black} ❯ %f%k "
  RPROMPT=''
  zle .reset-prompt
  PROMPT=$saved_prompt
  RPROMPT=$saved_rprompt

  if (( ret )); then
    zle .send-break
  else
    zle .accept-line
  fi

  return ret
}
zle -N zle-line-init

# general exports
export PATH=$PATH:~/.platformio/penv/bin
export DOTNET_CLI_UI_LANGUAGE=en
export COLORTERM=truecolor
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export LANG=en_CA.UTF-8
export XDG_CONFIG_HOME="$HOME/.config"

# aliases
alias git-log="git log --oneline --decorate --graph"
alias less="less -r"
alias diff="diff --color=always"
alias diffy="git diff --no-index"
alias catt='bat --paging=never'
alias ls="eza --icons=always --color=always"
#alias man="tldr"
alias ping="gping"
alias vim="nvim"
#alias cd="z"
alias brewup="brew update && brew upgrade"
alias git-branch-cleanup="git fetch && git checkout main && git pull origin main && git branch -D dev && git checkout -b dev && git push origin dev -f && git checkout staging && git pull origin main && git push origin staging && git checkout dev"
alias rick="curl http://ascii.live/rick"

# GPG settings
export "GPG_TTY=$(tty)"
export "SSH_AUTH_SOCK=${HOME}/.gnupg/S.gpg-agent.ssh"
gpgconf --launch gpg-agent

# Load rbenv automatically
eval "$(rbenv init - zsh)"
export GEM_HOME="$HOME/.gem"
export PATH="$HOME/.gem/ruby/2.6.0/bin:$PATH"

# fix for docker cli
export PATH="$HOME/.docker/bin:$PATH"

# dotnet sdk
export PATH="/usr/local/share/dotnet/x64:$PATH"

# tmuxifier
export PATH="$HOME/.config/tmux/plugins/tmuxifier/bin:$PATH"
export TMUXIFIER_LAYOUT_PATH="$HOME/.config/tmuxifier/layouts"
eval "$(tmuxifier init -)"

# NVM specific
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# zsh-autosuggestions
source $(brew --prefix)/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# zoxide
eval "$(zoxide init zsh)"

# history setup
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=100000
export SAVEHIST=100000
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY

# partial history search bindings
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

# jira-cli config
source "${XDG_CONFIG_HOME}/jira-cli/env"
alias jira-sprint='jira issue list -q "sprint in openSprints() and assignee = currentUser() and project in (\"TECH\", \"IBV\", \"CHECK\")"'

# starship
export STARSHIP_CONFIG="${XDG_CONFIG_HOME}/starship/starship.toml"
eval "$(starship init zsh)"

# fzf
source <(fzf --zsh)

# # kitty sessions
# function kt-session() {
#   export PROJECT_DIR=$2
#   kitty --session "~/.config/kitty/sessions/$1.conf"
# }

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
source /opt/homebrew/opt/chruby/share/chruby/chruby.sh
source /opt/homebrew/opt/chruby/share/chruby/auto.sh
chruby ruby-3.3.5 # run chruby to see actual version
